# Step Project Forkio https://helga.novyk.gitlab.io/step_project_forkio

## Team

- Olga Novyk (helga.novyk@gmail.com)
- Iryna Kuznetsova (rinahammer@gmail.com)

## Technologies list

- gulp
- gulp-concat
- gulp-autoprefixer
- gulp-minify
- gulp-clean-css
- gulp-clean
- gulp-imagemin
- gulp-rename
- browser-sync
- gulp-sass
- sass

## Task breakdown

Olga Novyk:

1. Created project structure.
2. Installed the necessary packages from npm.
3. Set up tasks for gulp.
4. Added breakpoints for adaptive & styles for adaptive container. 
5. Added markup and styles for Revolutionary Editor block (editor).
6. Added markup and styles for Here is what you get section (benefits).
7. Added markup and styles for Fork Subscription Pricing section (pricing).

Iryna Kuznetsova:

1. Added markup and SCSS-styles for Header with section (header).
2. Added markup and SCSS-styles for People are talking about section (section-people-talk).
3. Wrote script for burger-menu.
