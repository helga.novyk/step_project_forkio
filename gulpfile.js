// ------- Modules initialization ------

import gulp from "gulp";
import concat from "gulp-concat";
import autoprefixer from 'gulp-autoprefixer';
import minify from 'gulp-minify';
import cleanCSS from 'gulp-clean-css';
import clean from 'gulp-clean';
import imagemin from 'gulp-imagemin';
import rename from 'gulp-rename'
import browserSync from 'browser-sync';

import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);

// ----------- Tasks --------------

const htmlTask = () => {
    return gulp.src("./src/*.html")
        .pipe(gulp.dest("./dist"));              // переміщення файлу index.html до папки dist
};

const cssTask = () => {
    return gulp.src("./src/styles/*.scss")
        .pipe(sass().on('error', sass.logError)) // компіляція всіх файлів scss із папки src у формат css
        .pipe(concat("styles.css"))              // об'єднання цих css файлів в один - з назвою styles.css
        .pipe(autoprefixer({                     // додавання префіксів вендорів
			cascade: false
		}))
        .pipe(rename({ suffix: '.min' }))        // додавання до назви файлу суфікса min, таким чином створюється ще один файл css
        .pipe(cleanCSS({compatibility: 'ie8'}))  // мініфікація файлу styles.min.css. Його підключаємо в HTML.
        .pipe(gulp.dest("./dist/styles"));       // переміщення файлів styles.css і styles.min.css до папки dist
};

const jsTask = () => {
    return gulp.src("./src/scripts/**/*.js")
        .pipe(concat("script.js"))               // об'єднання всіх файлів .js із папки src в один з назвою script.js
        .pipe(minify({                           // створення нового файлу script.min.js і його мініфікація. Його підключаємо в HTML.
            ext: {
                src: '.js',
                min: '.min.js'
            },
        }))
        .pipe(gulp.dest("./dist/scripts"));      // переміщення файлів script.js і script.min.js до папки dist
};

const imageTask = () => {
    return gulp.src("./src/images/**/*.*")
        .pipe(imagemin())                        // мініфікація всіх зображень із папки src
        .pipe(gulp.dest("./dist/images"));       // переміщення мініфікованих зображень до папки dist
};

const cleanDist = () => {
    return gulp.src("./dist", {read: false})
        .pipe(clean());                          // очищення папки dist
};

const watchTask = () => {                        // відстеження змін у файлах та перезавантаження сторінки браузера
    gulp.watch("./src/*.html", htmlTask).on('all', browserSync.reload); 
    gulp.watch("./src/styles/**/*.{scss, sass, css}", cssTask).on('all', browserSync.reload);
    gulp.watch("./src/scripts/**/*.js", jsTask).on('all', browserSync.reload);
    gulp.watch("./src/images/**/*.*", imageTask).on('all', browserSync.reload);
};

const server = () => {
    browserSync.init({                            // запуск сервера
        server: {
            baseDir: "./dist"
        },
        notify: false
    });
};


gulp.task("html", htmlTask);
gulp.task("style", cssTask);
gulp.task("script", jsTask);
gulp.task("cleanDist", cleanDist);
gulp.task("browser-sync", server);
gulp.task("image", imageTask);

// -------------- Main tasks --------------

gulp.task("build", gulp.series(
    cleanDist,
    gulp.parallel(htmlTask, cssTask, jsTask, imageTask)
));

gulp.task("dev", gulp.series(
    gulp.parallel(htmlTask, cssTask, jsTask, imageTask),
    gulp.parallel(server, watchTask)
))
